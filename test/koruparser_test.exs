defmodule KoruParserTest do
  use ExUnit.Case
  doctest KoruParser

  test "original example from postprint.pdf" do
    require KoruParser.Grammar
    rules = [S: [:A, :A, 'c'], A: [nil]]
    g = KoruParser.Grammar.new(rules) 
    # this version of tokens resembles lexer output
    tokens = [{:word, 1, 'c'}, :end_of_parse]
    p2 = KoruParser.Grammar.parse(g, :S, tokens)
    expected = "digraph G {
  \"{:S => AAc*, 0}\" -> \"{:S => AA*c, 0, 0}\" [label=\":left\"];
  \"{:S => AAc*, 0}\" -> \"{'c', 0, 1}\" [label=\":right\"];
  \"{:S, 0, 1}\" -> \"{:S => AAc*, 0}\" [label=\":right\"];
  \"{:S => AA*c, 0}\" -> \"{:S => A*Ac, 0, 0}\" [label=\":left\"];
  \"{:S => AA*c, 0}\" -> \"{:A => *, 0, 0}\" [label=\":right\"];
  \"{:S => AA*c, 0, 0}\" -> \"{:S => AA*c, 0}\" [label=\":right\"];
  \"{:S => A*Ac, 0}\" -> \"{:A => *, 0, 0}\" [label=\":right\"];
  \"{:S => A*Ac, 0, 0}\" -> \"{:S => A*Ac, 0}\" [label=\":right\"];
  \"{:A => *, 0}\" -> \"{:epsilon, 0, 0}\" [label=\":right\"];
  \"{:A => *, 0, 0}\" -> \"{:A => *, 0}\" [label=\":right\"];
}\n"
    assert KoruParser.Tree.diagram(p2.sppf) == expected
  end
end
