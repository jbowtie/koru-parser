defmodule KoruParser.Tree do

  @moduledoc """
  This is a very naive implementation of a graph structure that has a list of
  nodes and a list of edges. It only implements enough functionality to back
  the GSS and SPPF trees needed by the parsing alorithm.

  In future, the internals are likely to forward to :digraph or similar library.
  """

  use GenServer

  #this part is the API
  def start_link do
    GenServer.start_link(__MODULE__, {[], []})
  end
  @doc "Add a node."
  def add_node(pid, graph_node) do
    GenServer.call(pid, {:add_node, graph_node})
  end
  @doc "Checks to see if a node is already in the graph."
  def member?(pid, graph_node) do
    GenServer.call(pid, {:has_node, graph_node})
  end
  @doc "Add a labelled edge between two nodes."
  def add_edge(pid, {from, label, to}) do
    GenServer.call(pid, {:add_edge, {from, label, to}})
  end
  @doc "Checks to see if an edge is in the graph."
  def edge?(pid, edge) do
    GenServer.call(pid, {:has_edge, edge})
  end
  @doc "Return all edges that originate from node `from`)"
  def get_edges(pid, from) do
    GenServer.call(pid, {:get_edges, from})
  end
  
  #Simply returns the constructed graph (as lists of nodes and edges)
  defp graph(pid) do
    GenServer.call(pid, :graph)
  end

  @doc """
  Produces DOT output suitable for use with graphvis and other tools.
  """
  def diagram(pid) do
  {_, e} = graph(pid)
  dg = Enum.reduce(e, "", fn {a,b,c}, g ->
    "#{g}  \"#{String.replace(Kernel.inspect(a), "\"", "'")}\" -> \"#{String.replace(Kernel.inspect(c), "\"", "'")}\" [label=\"#{inspect b}\"];\n"
  end)
  "digraph G {\n#{dg}}\n"
  end

  #this is that actual implementation
  def handle_call({:add_node, graph_node}, _from, {nodes, edges}) do
    state = {[graph_node | nodes], edges}
    {:reply, state, state}
  end
  def handle_call({:add_edge, edge}, _from, {nodes, edges}) do
    state = {nodes, [edge | edges]}
    {:reply, state, state}
  end
  def handle_call({:has_node, graph_node}, _from, {nodes, edges}) do
    {:reply, graph_node in nodes, {nodes, edges}}
  end
  def handle_call({:has_edge, edge}, _from, {nodes, edges}) do
    {:reply, edge in edges, {nodes, edges}}
  end
  def handle_call({:get_edges, n}, _from, {nodes, edges}) do
    e = Enum.filter(edges, fn({u, _, _}) -> u == n end)
    {:reply, e, {nodes, edges}}
  end
  def handle_call(:graph, _from, state) do
    {:reply, state, state}
  end
end
