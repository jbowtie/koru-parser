defmodule KoruParser.State do
  @moduledoc """
  This structure keeps track of the state used by the GLL algorithm.
  """
  alias KoruParser.Tree, as: Tree

  defstruct desc: MapSet.new, todo: [], popped: MapSet.new,
    stack: nil, sppf: nil, pos: 0, top: nil, current: nil

  def new do
    #initialize the GSS stack
    {:ok, stack} = Tree.start_link
    Tree.add_node(stack, {"L0", 0})
    #initialize the SPPF tree
    {:ok, forest} = Tree.start_link
    %KoruParser.State{stack: stack, sppf: forest}
  end

  @doc "Push a new descriptor onto the TODO list"
  def addDescriptor(state, descriptor) do
    %KoruParser.State{state |
      desc: MapSet.put(state.desc, descriptor),
      todo: [descriptor | state.todo]
    }
  end

  def next(state = %KoruParser.State{todo: []}) do
    {:next, state}
  end

  def next(state) do
    [{_slot, top, pos, sppf} | tail] = state.todo
    %KoruParser.State{state |
      todo: tail,
      pos: pos,
      top: top,
      current: sppf
    }
  end

  @doc "Pop the next item on the TODO list"
  def next_step(state = %KoruParser.State{todo: []}) do
    {nil, state}
  end
  def next_step(state) do
    [{slot, top, pos, sppf} | tail] = state.todo
    {slot, %KoruParser.State{state |
      todo: tail,
      pos: pos,
      top: top,
      current: sppf
      }
    }
  end

  def pop(state, stackNode) do
    %KoruParser.State{state |
      popped: MapSet.put(state.popped, stackNode)
    }
  end
  def getPopped(state, n) do
    Enum.filter(state.popped, fn({v, _}) -> v == n end)
  end
  def advance(state) do
    %KoruParser.State{state |
      pos: state.pos + 1
    }
  end
  def setCurrent(state, c) do
    %KoruParser.State{state |
      current: c
    }
  end
  def setTop(state, c) do
    %KoruParser.State{state |
      top: c
    }
  end
end
