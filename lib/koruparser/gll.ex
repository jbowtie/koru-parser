defmodule KoruParser.GLL do
  @moduledoc """
  This implements the core GLL algorithm as laid out by (references)
  """
  alias KoruParser.State, as: State
  alias KoruParser.Tree, as: Tree
  # add a descriptor to be processed (unless already seen)
  def add(state, slot, gssNode, pos, sppfNode) do
    d = {slot, gssNode, pos, sppfNode}
    if MapSet.member?(state.desc, d) do
      state
    else
      state |> State.addDescriptor(d)
    end
  end
  # create(L,u,i,w) in paper
  def pushGss(state, slot, gssNode, pos, sppfNode) do
    v = {slot, pos}
    Tree.add_node(state.stack, v) #unless already exists?
    if not Tree.edge?(state.stack, {v, sppfNode, gssNode}) do
      Tree.add_edge(state.stack, {v, sppfNode, gssNode})
      existing = State.getPopped(state, v)
      contingentPop(state, {slot, gssNode, sppfNode}, existing)
    else
      state
    end
  end
  def contingentPop(state, {_slot, _gssNode, _sppfNode}, []), do: state
  def contingentPop(state, {slot, gssNode, sppfNode}, [{_, z} | tail]) do
    y = packedNode(state, slot, sppfNode, z)
    {_, _, h} = z
    s = state |> add(slot, gssNode, h, y)
    contingentPop(s, {slot, gssNode, sppfNode}, tail)
  end


  def pop(state, u, inputPos, sppfNode) do
    {slot, _} = u
    s = state |> State.pop({u, sppfNode})
    edges = Tree.get_edges(s.stack, u)
    popEdges(s, {slot, inputPos, sppfNode}, edges)
  end

  def popEdges(state, {_slot, _inputPos, _sppfNode}, []), do: state
  def popEdges(state, {slot, inputPos, sppfNode}, [{_u,w,v} | tail]) do
    y = packedNode(state, slot, w, sppfNode)
    s = state |> add(slot, v, inputPos, y)
    popEdges(s, {slot, inputPos, sppfNode}, tail)
  end

  # only add the node if it doesn't already exist
  defp add_sppf_node(sppf, n) do
    unless Tree.member?(sppf, n) do
      Tree.add_node(sppf, n)
    end
  end
  # conditionally pack the edges
  defp packDelta(state, slot, y, k, right) do
    unless Tree.edge?(state.sppf, {y, :right, {slot, k}}) or Tree.edge?(state.sppf, {y, :left, {slot, k}}) do
      p = {slot, k}
      Tree.add_node(state.sppf, p)
      Tree.add_edge(state.sppf, {y, :right, p})
      Tree.add_edge(state.sppf, {p, :right, right})
    end
    y
  end
  defp packLR(state, slot, y, k, {left, right}) do
    unless Tree.edge?(state.sppf, {y, :right, {slot, k}}) or Tree.edge?(state.sppf, {y, :left, {slot, k}}) do
      p = {slot, k}
      Tree.add_node(state.sppf, p)
      Tree.add_edge(state.sppf, {y, :right, p})
      Tree.add_edge(state.sppf, {p, :right, right})
      Tree.add_edge(state.sppf, {p, :left, left})
    end
    y
  end
  def packedNode(state, slot, {s, j, k}, {q, k, i}) do
    if KoruParser.GrammarSlot.first?(slot) do
      {q, k, i}
    else
      y = if KoruParser.GrammarSlot.last?(slot) do
        {slot.lhs, j, i}
      else
        {slot, j, i}
      end
      add_sppf_node(state.sppf, y)
      packLR(state, slot, y, k, {{s,j,k}, {q,k,i}})
    end
  end
  def packedNode(state, slot, :delta, right) do
    if KoruParser.GrammarSlot.first?(slot) do
      right
    else
      {q, k, i} = right
      y = if KoruParser.GrammarSlot.last?(slot) do
        {slot.lhs, k, i}
      else
        {slot, k, i}
      end
      add_sppf_node(state.sppf, y)
      packDelta(state, slot, y, k, right)
    end
  end

  def terminal(a, i) do
    {a, i, i + 1}
  end
  def codeTerminal(state, symbol, newslot) do
    r = terminal(symbol, state.pos)
    Tree.add_node(state.sppf, r)
    s = state |> State.advance
    n = packedNode(s, newslot, s.current, r)
    s |> State.setCurrent(n)
  end

  def epsilon(i) do
    {:epsilon, i, i}
  end
  def codeEpsilon(state, newslot) do
    #symbol version
    r = epsilon(state.pos)
    Tree.add_node(state.sppf, r)
    n = packedNode(state, newslot, state.current, r)
    s = state |> State.setCurrent(n)
    #string version -- call symbol, pop, jump to main
    pop(s, s.top, s.pos, s.current)
  end

  def codeNonTerm(state, newslot) do
    v = {newslot, state.pos}
    state
      |> pushGss(newslot, state.top, state.pos, state.current)
      |> State.setCurrent(v)
  end
end

