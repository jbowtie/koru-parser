# represent a grammar slot as defined in the GLL papers
defmodule KoruParser.GrammarSlot do
  @moduledoc """
  This is the structure referred to as a slot in the GLL papers
  and as an item in the Earley papers
  """
  defstruct lhs: "", rhs: [], pos: 0
  def new(l, r, p) do
    %KoruParser.GrammarSlot{lhs: l, rhs: r, pos: p}
  end
  def first?(%KoruParser.GrammarSlot{rhs: [a, a | _], pos: 1}) do
    #true UNLESS A derives null
    false
  end
  def first?(slot) do
    slot.pos == 1
  end
  def last?(slot) do
    slot.pos >= Enum.count(slot.rhs)
  end
  def next_symbol(slot) do
      Enum.at(slot.rhs, slot.pos, :none)
  end
  defimpl Inspect, for: KoruParser.GrammarSlot do
    def inspect(slot, _) do
      dotted = List.insert_at(slot.rhs, slot.pos, "*")
      inspect(slot.lhs) <> " => " <> Enum.join(dotted, "")
    end
  end
end


defmodule KoruParser.Grammar do
  @moduledoc """
  Given a set of rules, derive a parser
  """
  defstruct rules: []
  def new(kw) do
    k = kw
    |> Keyword.keys
    |> Enum.uniq
    |> Enum.map(fn k -> {k, Keyword.get_values(kw, k)} end)
    %KoruParser.Grammar{rules: k}
  end
  def productions(g, rule) do
    g.rules[rule]
  end
  def derivesNil?(g, rule) do
    g
    |> productions(rule)
    |> Enum.any?(fn(x) -> derivesNil?(g, rule, x) end)
  end
  def derivesNil?(_g, _rule, [nil | _tail]) do
    true
  end
  def derivesNil?(g, rule, [head | tail]) when is_atom(head) do
    derivesNil?(g, head) && derivesNil?(g, rule, tail)
  end
  def derivesNil?(_g, _rule, []) do
    true
  end
  def derivesNil?(_g, _rule, _rhs) do
    false
  end

  defp testSelect(token, slot, g) do
    nexts = KoruParser.GrammarSlot.next_symbol(slot)
    case nexts do
      ^token -> true
      nil -> true
      :none -> false
      # need to look at testSelect defn again to see if we want to call differently?
      rule when is_atom(rule) -> productions(g, rule) |> Enum.any?(fn rhs -> testSelect(token, KoruParser.GrammarSlot.new(rule, rhs, 0), g) end)
      _ -> false
    end
  end

  defp addAlternates(state, _g, _rule, []),  do: state
  defp addAlternates(state, g, rule, [head | tail]) do
    new_slot = KoruParser.GrammarSlot.new(rule, head, 0)
    state
      |> KoruParser.GLL.add(new_slot, state.current, state.pos, :delta)
      |> addAlternates(g, rule, tail) 
  end

  # the rule matches an epsilon
  defp codeSlot(state, slot, token, nil, g) do
    state 
    |> KoruParser.GLL.codeEpsilon(KoruParser.GrammarSlot.new(slot.lhs, [nil], 0))
    |> parse_token(token, g)
  end

  # we've reached the end of a rule
  defp codeSlot(state, _slot, token, :none, g) do
    state
    |> KoruParser.GLL.pop(state.top, state.pos, state.current)
    |> parse_token(token, g)
  end

  # the next symbol in a rule is a non-terminal
  defp codeSlot(state, slot, token, rule, g) when is_atom(rule) do
    s = KoruParser.GLL.codeNonTerm(state, KoruParser.GrammarSlot.new(slot.lhs, slot.rhs, slot.pos + 1))
    s |> addAlternates(g, rule, productions(g, rule))
      |> parse_token(token, g)
  end

  # the next symbol in a rule matches the token
  defp codeSlot(state, slot, token, token, _g) do
    new_slot = KoruParser.GrammarSlot.new(slot.lhs, slot.rhs, slot.pos + 1)
    s = KoruParser.GLL.codeTerminal(state, token, new_slot)
    s |> KoruParser.GLL.add(new_slot, s.current, s.pos, s.current)
  end

  # ensure we pop any final item in the todo list 
  def parse_token(state, :end_of_parse, _g) do
    #IO.puts "END OF PARSE"
    {_, s} = state |> KoruParser.State.next_step()
    s
  end

  #handle the case where we're passed leex-style token
  def parse_token(state, {_type, _line, token}, g) do
    {next_slot, s} = state |> KoruParser.State.next_step()
    #IO.puts "parsing #{inspect next_slot}"
    if testSelect(token, next_slot, g) do
      nexts = KoruParser.GrammarSlot.next_symbol(next_slot)
      codeSlot(s, next_slot, token, nexts, g)
    else
      s
    end
  end

  #handle a simple string token
  def parse_token(state, token, g) do
    {next_slot, s} = state |> KoruParser.State.next_step()
    #IO.puts "parsing #{inspect next_slot}"
    if testSelect(token, next_slot, g) do
      nexts = KoruParser.GrammarSlot.next_symbol(next_slot)
      codeSlot(s, next_slot, token, nexts, g)
    else
      s
    end
  end
  def parse(g, start, tokens) do
    [rhs | _] = productions(g, start)
    slot = KoruParser.GrammarSlot.new(start, rhs, 0)
    s = KoruParser.State.new
        |> KoruParser.GLL.add(slot, {"L0", 0}, 0, :delta)
    Enum.reduce(tokens, s, fn token, state ->
      parse_token(state, token, g)
    end)
  end
end

